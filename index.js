"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.script = exports.samp = exports.s = exports.ruby = exports.rt = exports.rp = exports.q = exports.progress = exports.pre = exports.plaintext = exports.param = exports.p = exports.output = exports.option = exports.optgroup = exports.ol = exports.object = exports.noscript = exports.noframes = exports.noembed = exports.nobr = exports.nextid = exports.nav = exports.meter = exports.meta = exports.menu = exports.math = exports.marquee = exports.mark = exports.map = exports.listing = exports.link = exports.li = exports.legend = exports.label = exports.keygen = exports.kbd = exports.isindex = exports.ins = exports.input = exports.img = exports.iframe = exports.i = exports.html = exports.hr = exports.hgroup = exports.header = exports.head = exports.h6 = exports.h5 = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.frameset = exports.frame = exports.form = exports.footer = exports.font = exports.figure = exports.figcaption = exports.fieldset = exports.embed = exports.em = exports.dt = exports.dl = exports.div = exports.dfn = exports.details = exports.del = exports.dd = exports.datalist = exports.command = exports.colgroup = exports.col = exports.code = exports.cite = exports.center = exports.caption = exports.canvas = exports.button = exports.br = exports.body = exports.blockquote = exports.blink = exports.big = exports.bgsound = exports.bdo = exports.basefont = exports.base = exports.b = exports.audio = exports.aside = exports.article = exports.area = exports.applet = exports.address = exports.acronym = exports.abbr = exports.a = void 0;
exports.xmp = exports.wbr = exports.video = exports.var_ = exports.ul = exports.u = exports.tt = exports.track = exports.tr = exports.title = exports.time = exports.thead = exports.th = exports.tfoot = exports.textarea = exports.td = exports.tbody = exports.table = exports.svg = exports.summary = exports.sup = exports.sub = exports.style = exports.strong = exports.strike = exports.span = exports.spacer = exports.source = exports.small = exports.select = exports.section = void 0;

const isNode = x => x instanceof Node;

const isAttributes = x => !(x instanceof Array) && x instanceof Object,
      isChildren = x => x instanceof Array && x.every(item => isNode(item)),
      isText = x => typeof x === 'string',
      isNone = x => x === undefined;

const getArgs = (arg1, arg2) => isNone(arg1) && isNone(arg2) ? {
  attributes: {},
  children: []
} : isChildren(arg1) && isNone(arg2) ? {
  attributes: {},
  children: arg1
} : isAttributes(arg1) && isNone(arg2) ? {
  attributes: arg1,
  children: []
} : isText(arg1) && isNone(arg2) ? {
  attributes: {},
  children: [document.createTextNode(arg1)]
} : isAttributes(arg1) && isText(arg2) ? {
  attributes: arg1,
  children: [document.createTextNode(arg2)]
} : isAttributes(arg1) && isChildren(arg2) ? {
  attributes: arg1,
  children: arg2
} : null;

const setAttribute = (element, key) => value => {
  switch (key) {
    case 'id':
      element.id = value;
      break;

    case 'className':
      element.className = value;
      break;

    case 'classSet':
      element.className = [element.className.trim()].concat(Object.entries(value).map(([className, isIncluded]) => isIncluded ? className : '')).filter(e => !!e).join(' ');
      break;

    case 'dataset':
      Object.entries(value).forEach(([key, value]) => {
        element.dataset[key] = value;
      });
      break;

    case 'style':
      Object.entries(value).forEach(([property, value]) => {
        element.style[property] = value;
      });
      break;

    case 'contentEditable':
      element.contentEditable = value ? 'true' : 'false';
      break;

    default:
      element[key] = value;
  }
};

const setAttributes = element => attributes => {
  for (const [key, value] of Object.entries(attributes)) {
    setAttribute(element, key)(value);
  }
};

const removeChildren = element => {
  while (element.firstChild) element.removeChild(element.firstChild);
};

const setChildren = element => children => {
  removeChildren(element);
  children.forEach((_child, index) => {
    var {
      child,
      child$
    } = {
      child: isNode(_child) ? _child : isText(_child) ? document.createTextNode(_child) : null
    };
    if (child) element.appendChild(child);
  });
};

const setElement = (element, {
  attributes,
  children
}) => {
  setAttributes(element)(attributes);
  setChildren(element)(children);
  return element;
};

const _element = tagName => (arg1, arg2) => {
  const args = getArgs(arg1, arg2);
  if (args === null) throw new Error('Invalid arguments passed to ddom');
  return setElement(document.createElement(tagName), args);
};

const a = _element('a');

exports.a = a;

const abbr = _element('abbr');

exports.abbr = abbr;

const acronym = _element('acronym');

exports.acronym = acronym;

const address = _element('address');

exports.address = address;

const applet = _element('applet');

exports.applet = applet;

const area = _element('area');

exports.area = area;

const article = _element('article');

exports.article = article;

const aside = _element('aside');

exports.aside = aside;

const audio = _element('audio');

exports.audio = audio;

const b = _element('b');

exports.b = b;

const base = _element('base');

exports.base = base;

const basefont = _element('basefont');

exports.basefont = basefont;

const bdo = _element('bdo');

exports.bdo = bdo;

const bgsound = _element('bgsound');

exports.bgsound = bgsound;

const big = _element('big');

exports.big = big;

const blink = _element('blink');

exports.blink = blink;

const blockquote = _element('blockquote');

exports.blockquote = blockquote;

const body = _element('body');

exports.body = body;

const br = _element('br');

exports.br = br;

const button = _element('button');

exports.button = button;

const canvas = _element('canvas');

exports.canvas = canvas;

const caption = _element('caption');

exports.caption = caption;

const center = _element('center');

exports.center = center;

const cite = _element('cite');

exports.cite = cite;

const code = _element('code');

exports.code = code;

const col = _element('col');

exports.col = col;

const colgroup = _element('colgroup');

exports.colgroup = colgroup;

const command = _element('command');

exports.command = command;

const datalist = _element('datalist');

exports.datalist = datalist;

const dd = _element('dd');

exports.dd = dd;

const del = _element('del');

exports.del = del;

const details = _element('details');

exports.details = details;

const dfn = _element('dfn');

exports.dfn = dfn;

const div = _element('div');

exports.div = div;

const dl = _element('dl');

exports.dl = dl;

const dt = _element('dt');

exports.dt = dt;

const em = _element('em');

exports.em = em;

const embed = _element('embed');

exports.embed = embed;

const fieldset = _element('fieldset');

exports.fieldset = fieldset;

const figcaption = _element('figcaption');

exports.figcaption = figcaption;

const figure = _element('figure');

exports.figure = figure;

const font = _element('font');

exports.font = font;

const footer = _element('footer');

exports.footer = footer;

const form = _element('form');

exports.form = form;

const frame = _element('frame');

exports.frame = frame;

const frameset = _element('frameset');

exports.frameset = frameset;

const h1 = _element('h1');

exports.h1 = h1;

const h2 = _element('h2');

exports.h2 = h2;

const h3 = _element('h3');

exports.h3 = h3;

const h4 = _element('h4');

exports.h4 = h4;

const h5 = _element('h5');

exports.h5 = h5;

const h6 = _element('h6');

exports.h6 = h6;

const head = _element('head');

exports.head = head;

const header = _element('header');

exports.header = header;

const hgroup = _element('hgroup');

exports.hgroup = hgroup;

const hr = _element('hr');

exports.hr = hr;

const html = _element('html');

exports.html = html;

const i = _element('i');

exports.i = i;

const iframe = _element('iframe');

exports.iframe = iframe;

const img = _element('img');

exports.img = img;

const input = _element('input');

exports.input = input;

const ins = _element('ins');

exports.ins = ins;

const isindex = _element('isindex');

exports.isindex = isindex;

const kbd = _element('kbd');

exports.kbd = kbd;

const keygen = _element('keygen');

exports.keygen = keygen;

const label = _element('label');

exports.label = label;

const legend = _element('legend');

exports.legend = legend;

const li = _element('li');

exports.li = li;

const link = _element('link');

exports.link = link;

const listing = _element('listing');

exports.listing = listing;

const map = _element('map');

exports.map = map;

const mark = _element('mark');

exports.mark = mark;

const marquee = _element('marquee');

exports.marquee = marquee;

const math = _element('math');

exports.math = math;

const menu = _element('menu');

exports.menu = menu;

const meta = _element('meta');

exports.meta = meta;

const meter = _element('meter');

exports.meter = meter;

const nav = _element('nav');

exports.nav = nav;

const nextid = _element('nextid');

exports.nextid = nextid;

const nobr = _element('nobr');

exports.nobr = nobr;

const noembed = _element('noembed');

exports.noembed = noembed;

const noframes = _element('noframes');

exports.noframes = noframes;

const noscript = _element('noscript');

exports.noscript = noscript;

const object = _element('object');

exports.object = object;

const ol = _element('ol');

exports.ol = ol;

const optgroup = _element('optgroup');

exports.optgroup = optgroup;

const option = _element('option');

exports.option = option;

const output = _element('output');

exports.output = output;

const p = _element('p');

exports.p = p;

const param = _element('param');

exports.param = param;

const plaintext = _element('plaintext');

exports.plaintext = plaintext;

const pre = _element('pre');

exports.pre = pre;

const progress = _element('progress');

exports.progress = progress;

const q = _element('q');

exports.q = q;

const rp = _element('rp');

exports.rp = rp;

const rt = _element('rt');

exports.rt = rt;

const ruby = _element('ruby');

exports.ruby = ruby;

const s = _element('s');

exports.s = s;

const samp = _element('samp');

exports.samp = samp;

const script = _element('script');

exports.script = script;

const section = _element('section');

exports.section = section;

const select = _element('select');

exports.select = select;

const small = _element('small');

exports.small = small;

const source = _element('source');

exports.source = source;

const spacer = _element('spacer');

exports.spacer = spacer;

const span = _element('span');

exports.span = span;

const strike = _element('strike');

exports.strike = strike;

const strong = _element('strong');

exports.strong = strong;

const style = _element('style');

exports.style = style;

const sub = _element('sub');

exports.sub = sub;

const sup = _element('sup');

exports.sup = sup;

const summary = _element('summary');

exports.summary = summary;

const svg = _element('svg');

exports.svg = svg;

const table = _element('table');

exports.table = table;

const tbody = _element('tbody');

exports.tbody = tbody;

const td = _element('td');

exports.td = td;

const textarea = _element('textarea');

exports.textarea = textarea;

const tfoot = _element('tfoot');

exports.tfoot = tfoot;

const th = _element('th');

exports.th = th;

const thead = _element('thead');

exports.thead = thead;

const time = _element('time');

exports.time = time;

const title = _element('title');

exports.title = title;

const tr = _element('tr');

exports.tr = tr;

const track = _element('track');

exports.track = track;

const tt = _element('tt');

exports.tt = tt;

const u = _element('u');

exports.u = u;

const ul = _element('ul');

exports.ul = ul;

const var_ = _element('var');

exports.var_ = var_;

const video = _element('video');

exports.video = video;

const wbr = _element('wbr');

exports.wbr = wbr;

const xmp = _element('xmp');

exports.xmp = xmp;