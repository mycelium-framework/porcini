const isNode = x => x instanceof Node
const isAttributes = x => !(x instanceof Array) && x instanceof Object,
  isChildren = x => x instanceof Array && x.every(item => isNode(item)),
  isText = x => typeof x === 'string',
  isNone = x => x === undefined

const getArgs = (arg1, arg2) =>
  isNone(arg1) && isNone(arg2)
    ? { attributes: {}, children: [] }
    : isChildren(arg1) && isNone(arg2)
    ? { attributes: {}, children: arg1 }
    : isAttributes(arg1) && isNone(arg2)
    ? { attributes: arg1, children: [] }
    : isText(arg1) && isNone(arg2)
    ? { attributes: {}, children: [document.createTextNode(arg1)] }
    : isAttributes(arg1) && isText(arg2)
    ? { attributes: arg1, children: [document.createTextNode(arg2)] }
    : isAttributes(arg1) && isChildren(arg2)
    ? { attributes: arg1, children: arg2 }
    : null

const setAttribute = (element, key) => value => {
  switch (key) {
    case 'id':
      element.id = value
      break
    case 'className':
      element.className = value
      break
    case 'classSet':
      element.className = [element.className.trim()]
        .concat(
          Object.entries(value).map(([className, isIncluded]) => (isIncluded ? className : ''))
        )
        .filter(e => !!e)
        .join(' ')
      break
    case 'dataset':
      Object.entries(value).forEach(([key, value]) => {
        element.dataset[key] = value
      })
      break
    case 'style':
      Object.entries(value).forEach(([property, value]) => {
        element.style[property] = value
      })
      break
    case 'contentEditable':
      element.contentEditable = value ? 'true' : 'false'
      break
    default:
      element[key] = value
  }
}

const setAttributes = element => attributes => {
  for (const [key, value] of Object.entries(attributes)) {
    setAttribute(element, key)(value)
  }
}

const removeChildren = element => {
  while (element.firstChild) element.removeChild(element.firstChild)
}

const setChildren = element => children => {
  removeChildren(element)

  children.forEach((_child, index) => {
    var { child, child$ } = {
      child: isNode(_child) ? _child : isText(_child) ? document.createTextNode(_child) : null
    }

    if (child) element.appendChild(child)
  })
}

const setElement = (element, { attributes, children }) => {
  setAttributes(element)(attributes)
  setChildren(element)(children)
  return element
}

const _element = tagName => (arg1, arg2) => {
  const args = getArgs(arg1, arg2)
  if (args === null) throw new Error('Invalid arguments passed to ddom')
  return setElement(document.createElement(tagName), args)
}

const a = _element('a')
const abbr = _element('abbr')
const acronym = _element('acronym')
const address = _element('address')
const applet = _element('applet')
const area = _element('area')
const article = _element('article')
const aside = _element('aside')
const audio = _element('audio')
const b = _element('b')
const base = _element('base')
const basefont = _element('basefont')
const bdo = _element('bdo')
const bgsound = _element('bgsound')
const big = _element('big')
const blink = _element('blink')
const blockquote = _element('blockquote')
const body = _element('body')
const br = _element('br')
const button = _element('button')
const canvas = _element('canvas')
const caption = _element('caption')
const center = _element('center')
const cite = _element('cite')
const code = _element('code')
const col = _element('col')
const colgroup = _element('colgroup')
const command = _element('command')
const datalist = _element('datalist')
const dd = _element('dd')
const del = _element('del')
const details = _element('details')
const dfn = _element('dfn')
const div = _element('div')
const dl = _element('dl')
const dt = _element('dt')
const em = _element('em')
const embed = _element('embed')
const fieldset = _element('fieldset')
const figcaption = _element('figcaption')
const figure = _element('figure')
const font = _element('font')
const footer = _element('footer')
const form = _element('form')
const frame = _element('frame')
const frameset = _element('frameset')
const h1 = _element('h1')
const h2 = _element('h2')
const h3 = _element('h3')
const h4 = _element('h4')
const h5 = _element('h5')
const h6 = _element('h6')
const head = _element('head')
const header = _element('header')
const hgroup = _element('hgroup')
const hr = _element('hr')
const html = _element('html')
const i = _element('i')
const iframe = _element('iframe')
const img = _element('img')
const input = _element('input')
const ins = _element('ins')
const isindex = _element('isindex')
const kbd = _element('kbd')
const keygen = _element('keygen')
const label = _element('label')
const legend = _element('legend')
const li = _element('li')
const link = _element('link')
const listing = _element('listing')
const map = _element('map')
const mark = _element('mark')
const marquee = _element('marquee')
const math = _element('math')
const menu = _element('menu')
const meta = _element('meta')
const meter = _element('meter')
const nav = _element('nav')
const nextid = _element('nextid')
const nobr = _element('nobr')
const noembed = _element('noembed')
const noframes = _element('noframes')
const noscript = _element('noscript')
const object = _element('object')
const ol = _element('ol')
const optgroup = _element('optgroup')
const option = _element('option')
const output = _element('output')
const p = _element('p')
const param = _element('param')
const plaintext = _element('plaintext')
const pre = _element('pre')
const progress = _element('progress')
const q = _element('q')
const rp = _element('rp')
const rt = _element('rt')
const ruby = _element('ruby')
const s = _element('s')
const samp = _element('samp')
const script = _element('script')
const section = _element('section')
const select = _element('select')
const small = _element('small')
const source = _element('source')
const spacer = _element('spacer')
const span = _element('span')
const strike = _element('strike')
const strong = _element('strong')
const style = _element('style')
const sub = _element('sub')
const sup = _element('sup')
const summary = _element('summary')
const svg = _element('svg')
const table = _element('table')
const tbody = _element('tbody')
const td = _element('td')
const textarea = _element('textarea')
const tfoot = _element('tfoot')
const th = _element('th')
const thead = _element('thead')
const time = _element('time')
const title = _element('title')
const tr = _element('tr')
const track = _element('track')
const tt = _element('tt')
const u = _element('u')
const ul = _element('ul')
const var_ = _element('var')
const video = _element('video')
const wbr = _element('wbr')
const xmp = _element('xmp')

export {
  a,
  abbr,
  acronym,
  address,
  applet,
  area,
  article,
  aside,
  audio,
  b,
  base,
  basefont,
  bdo,
  bgsound,
  big,
  blink,
  blockquote,
  body,
  br,
  button,
  canvas,
  caption,
  center,
  cite,
  code,
  col,
  colgroup,
  command,
  datalist,
  dd,
  del,
  details,
  dfn,
  div,
  dl,
  dt,
  em,
  embed,
  fieldset,
  figcaption,
  figure,
  font,
  footer,
  form,
  frame,
  frameset,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  head,
  header,
  hgroup,
  hr,
  html,
  i,
  iframe,
  img,
  input,
  ins,
  isindex,
  kbd,
  keygen,
  label,
  legend,
  li,
  link,
  listing,
  map,
  mark,
  marquee,
  math,
  menu,
  meta,
  meter,
  nav,
  nextid,
  nobr,
  noembed,
  noframes,
  noscript,
  object,
  ol,
  optgroup,
  option,
  output,
  p,
  param,
  plaintext,
  pre,
  progress,
  q,
  rp,
  rt,
  ruby,
  s,
  samp,
  script,
  section,
  select,
  small,
  source,
  spacer,
  span,
  strike,
  strong,
  style,
  sub,
  sup,
  summary,
  svg,
  table,
  tbody,
  td,
  textarea,
  tfoot,
  th,
  thead,
  time,
  title,
  tr,
  track,
  tt,
  u,
  ul,
  var_,
  video,
  wbr,
  xmp
}
