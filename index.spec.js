"use strict";

var _ = require(".");

test('it should create an element with text', () => {
  const element = (0, _.span)('hello');
  expect(element).not.toBeNull();
  expect(element.innerHTML).toBe('hello');
});
test('it should create an element with text and attrs', () => {
  const element = (0, _.span)({
    id: 'foo',
    className: 'bar'
  }, 'hello');
  expect(element).not.toBeNull();
  expect(element.id).toBe('foo');
  expect(element.className).toBe('bar');
  expect(element.innerHTML).toBe('hello');
});
test('it should create an element with text, attrs and children', () => {
  const element = (0, _.span)({
    id: 'foo',
    className: 'bar'
  }, [(0, _.i)('hello '), (0, _.em)('world')]);
  expect(element).not.toBeNull();
  expect(element.id).toBe('foo');
  expect(element.className).toBe('bar');
  expect(element.childNodes.length).toBe(2);
  expect(element.textContent).toBe('hello world');
});