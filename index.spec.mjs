import { span, i, em } from '.'

test('it should create an element with text', () => {
  const element = span('hello')
  expect(element).not.toBeNull()
  expect(element.innerHTML).toBe('hello')
})


test('it should create an element with text and attrs', () => {
  const element = span({ id: 'foo', className: 'bar' }, 'hello')
  expect(element).not.toBeNull()
  expect(element.id).toBe('foo')
  expect(element.className).toBe('bar')
  expect(element.innerHTML).toBe('hello')
})

test('it should create an element with text, attrs and children', () => {
  const element = span({ id: 'foo', className: 'bar' }, [i('hello '), em('world')])
  expect(element).not.toBeNull()
  expect(element.id).toBe('foo')
  expect(element.className).toBe('bar')
  expect(element.childNodes.length).toBe(2)
  expect(element.textContent).toBe('hello world')
})
